path = "data.txt"
data = []

# read from path
with open(path) as f:
    for line in f:
        data.append(line)

# make all items like '712345678910'
for n in range(len(data)):
    length = len(data[n])
    phone = ""
    i = 0
    while i < length:
        if '0' <= data[n][i] <= '9':
            phone += data[n][i]
        i += 1
    data[n] = phone


result = []
# remove duplicates
for n in data:
    if n not in result:
        result.append(n)

# write final result
f = open('result.txt', 'w' )
for number in result:
    f.write("%s\n" % number)
f.close()